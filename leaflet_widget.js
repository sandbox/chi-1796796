(function ($) {
    Drupal.behaviors.leaflet_widget = {
        attach:function (context, settings) {
            var mapIncrement = 0;
            for (var field in settings.leaflet_widget)
                if (settings.leaflet_widget.hasOwnProperty(field)) {
                    new Drupal.behaviors.leaflet_widget.LeafletWidgetFieldController().init(
                        Drupal.settings.leaflet[mapIncrement++].lMap,
                        settings.leaflet_widget[field]
                    );
                }
        }
    };

    Drupal.behaviors.leaflet_widget.LeafletWidgetFieldController = function () {
    };
    Drupal.behaviors.leaflet_widget.LeafletWidgetFieldController.prototype = {
        constructor:Drupal.behaviors.leaflet_widget.LeafletWidgetFieldController,
        init:function (map, settings) {
            this._map = map;
            if (!this._map) {
                console.error('Map is not defined');
                return;
            }
            this._features = [];
            this._cardinality = settings.cardinality == -1
                ? Infinity
                : settings.cardinality;

            this._wktInputField = $(this._map.getContainer()).parents('div.field-type-geofield').find('.geofield_wkt');
            this._wktConverter = new L.WKT();

            this._map.addControl(L.control.draw());
            this._map.on('feature-added', this._onNewFeatureEventHandler, this);
            this._map.on('feature-removed', this._onFeatureRemoveHandler, this);

            if(this._wktInputField.val().length == 0) {
                //center map on default coordinates
                this._map.setView([settings.default_center.lat, settings.default_center.lon], settings.default_center.zoom);
            } else {
                //map will be centered on features
                this._getWKTFromField();
            }

        },
        _onNewFeatureEventHandler:function (event) {
            event.feature.on('edit', this._updateWKTField, this);
            this._addNewFeature(event.feature);
            this._updateWKTField();
        },
        _onFeatureRemoveHandler:function (event) {
            for (var i = 0, l = this._features.length; i < l; ++i) {
                if (this._features[i] === event.feature) {
                    this._features.splice(i, 1);
                    break;
                }
            }
            this._updateWKTField();
        },
        _addNewFeature:function (feature) {
            this._features.push(feature);
            if (this._features.length > this._cardinality) {
                var removedFeatures = this._features.splice(0, this._features.length - this._cardinality);
                for (var i = 0, l = removedFeatures.length; i < l; i++) {
                    this._map.removeLayer(removedFeatures[i]);
                }
            }
        },
        _getWKTFromField:function () {
            var wktString = this._wktInputField.val();
            this._features = this._wktConverter.read(wktString);

            if (this._features.constructor !== Array)
                this._features = [this._features];
            //Convert leaflet vector features to drawable features
            for (var i = 0, l = this._features.length; i < l; i++) {
                this._features[i] =  L.DrawHandler.convertFeatureToDrawable(this._map, this._features[i]);
            }
            this._addFeaturesOnMap();
            this._centerMapOnFeatures();
        },
        _addFeaturesOnMap:function () {
            for (var i = 0, l = this._features.length; i < l; ++i) {
                //Convert LatLng to marker
                if (this._features[i] instanceof L.marker) {
                    this._features[i].on('dragend', this._updateWKTField, this);
                } else {
                    this._features[i].on('edit', this._updateWKTField, this);
                }
                if (this._features[i].editing)
                    this._features[i].editing.enable();

                this._features[i].addTo(this._map);
            }
        },
        _centerMapOnFeatures:function () {
            this._map.fitBounds(L.featureGroup(this._features).getBounds());
        },
        _updateWKTField:function () {
            if (this._features.length == 0)
                this._wktInputField.val('');
            else
                this._wktInputField.val(this._wktConverter.write(
                    //We should not write array, if features includes just one object.
                    this._features.length > 1 ? this._features : this._features[0]
                ));
        }
    };
})(jQuery);