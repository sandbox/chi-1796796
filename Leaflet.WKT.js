L.WKT = L.Class.extend({

    /**
     * Taken from OpenLayers.
     */
    initialize: function(options) {
        this.regExes = {
            'typeStr': /^\s*(\w+)\s*\(\s*(.*)\s*\)\s*$/,
            'spaces': /\s+/,
            'parenComma': /\)\s*,\s*\(/,
            'doubleParenComma': /\)\s*\)\s*,\s*\(\s*\(/,  // can't use {2} here
            'trimParens': /^\s*\(?(.*?)\)?\s*$/
        };
    },

    /**
     * Method: read
     * Deserialize a WKT string and return a vector feature or an
     * array of vector features.  Supports WKT for POINT, MULTIPOINT,
     * LINESTRING, MULTILINESTRING, POLYGON, MULTIPOLYGON, and
     * GEOMETRYCOLLECTION.
     *
     * Parameters:
     * wkt - {String} A WKT string
     *
     */
    read: function(wkt) {
        var features, type, str;
        wkt = wkt.replace(/[\n\r]/g, " ");
        var matches = this.regExes.typeStr.exec(wkt);
        if(matches) {
            type = matches[1].toLowerCase();
            str = matches[2];
            if(this.parse[type]) {
                features = this.parse[type].apply(this, [str]);
            }
        }
        return features;
    },

    /**
     * Method: write
     * Serialize a feature or array of features into a WKT string.
     *
     * Parameters:
     * features - A feature or array of
     *            features
     *
     * Returns:
     * {String} The WKT string representation of the input geometries
     */
    write: function(features) {
        var collection, geometry, isCollection;
        if (features.constructor == Array) {
            collection = features;
            isCollection = true;
        } else {
            collection = [features];
            isCollection = false;
        }
        var pieces = [];
        if (isCollection) {
            pieces.push('GEOMETRYCOLLECTION(');
        }
        for (var i=0, len=collection.length; i<len; ++i) {
            if (isCollection && i>0) {
                pieces.push(',');
            }
            pieces.push(this.extractGeometry(collection[i], this.getGeometryName(collection[i])));
        }
        if (isCollection) {
            pieces.push(')');
        }
        return pieces.join('');
    },
    getGeometryName: function(geometry){
        if(geometry instanceof L.LatLng || geometry instanceof L.Marker)
            return 'point';
        if(geometry instanceof L.Polygon)
            return 'polygon';
        if(geometry instanceof L.Polyline)
            return 'linestring';
        if(geometry instanceof L.MultiPolygon)
            return 'multipolygon'
    },
    /**
     * Method: extractGeometry
     * Entry point to construct the WKT for a single Geometry object.
     *
     * Parameters:
     *
     * Returns:
     * {String} A WKT string of representing the geometry
     */
    extractGeometry: function(geometry, type) {
        if (!this.extract[type]) {
            return null;
        }
        if (this.internalProjection && this.externalProjection) {
            geometry = geometry.clone();
            geometry.transform(this.internalProjection, this.externalProjection);
        }
        var wktType = type == 'collection' ? 'GEOMETRYCOLLECTION' : type.toUpperCase();
        var data = wktType + '(' + this.extract[type].apply(this, [geometry]) + ')';
        return data;
    },

    /**
     * Object with properties corresponding to the geometry types.
     * Property values are functions that do the actual data extraction.
     */
    extract: {
        /**
         * Return a space delimited string of point coordinates.
         * @param {L.LatLng|L.Marker} point
         * @returns {String} A string of coordinates representing the point
         */
        'point': function(point) {
            if(point instanceof L.Marker)
                point = point.getLatLng();
            return point.lng + ' ' + point.lat;
        },

        /**
         NOT IMPLEMENTED

        'multipoint': function(multipoint) {
            var array = [];
            for(var i=0, len=multipoint.components.length; i<len; ++i) {
                array.push('(' +
                    this.extract.point.apply(this, [multipoint.components[i]]) +
                    ')');
            }
            return array.join(',');
        },*/

        /**
         * Return a comma delimited string of point coordinates from a line.
         * @param {L.Polyline} polyline
         * @returns {String} A string of point coordinate strings representing
         *                  the linestring
         */
        'linestring': function(linestring) {
            var array = [];
            for(var i=0, len=linestring._latlngs.length; i<len; ++i) {
                array.push(this.extract.point.apply(this, [linestring._latlngs[i]]));
            }
            return array.join(',');
        },

        /**
         NOT IMPLEMENTED

        'multilinestring': function(multilinestring) {
            var array = [];
            for(var i=0, len=multilinestring.components.length; i<len; ++i) {
                array.push('(' +
                    this.extract.linestring.apply(this, [multilinestring.components[i]]) +
                    ')');
            }
            return array.join(',');
        },*/

        /**
         * Return a comma delimited string of linear ring arrays from a polygon.
         * @param {L.Polygon} polygon
         * @returns {String} An array of linear ring arrays representing the polygon
         */
        'polygon':function (polygon) {
            var array = [],
                polygonString = '';
            for (var i = 0, len = polygon._latlngs.length; i < len; ++i) {
                array.push(this.extract.point.apply(this, [polygon._latlngs[i]]))
            }
            //need this, because leaflet polygons does not contains last point, equals the first
            array.push(array[0]);
            polygonString += '(' + array.join(',') + ')';
            array = [];

            if(polygon._holes) {
                //adding holes to result string
                var holesCoordinates = [], // line with holes coordinates
                    holesCoordinatesString = '';
                for (i = 0, len = polygon._holes.length; i < len; ++i) {

                    holesCoordinates = polygon._holes[i].concat(polygon._holes[i][0]);
                    holesCoordinatesString = this.extract.linestring.call(this, {_latlngs:holesCoordinates});
                    array.push(holesCoordinatesString);
                    polygonString += ',(' + array.join(',') + ')';
                }
            }
            return polygonString;
        },

        /**
         * Return an array of polygon arrays from a multipolygon.
         * @param {L.MultiPolygon} multipolygon
         * @returns {String} An array of polygon arrays representing
         *                  the multipolygon
         */
        'multipolygon': function(multipolygon) {
            var array = [];
            for(var layer in multipolygon._layers) {
                array.push('(' +
                    this.extract.polygon.apply(this, [multipolygon._layers[layer]]) +
                    ')');
            }
            return array.join(',');
        }

    },

    /**
     * Object with properties corresponding to the geometry types.
     * Property values are functions that do the actual parsing.
     */
    parse: {
        /**
         * Return point feature given a point WKT fragment.
         * @param {String} str A WKT fragment representing the point
         * @returns {L.LatLng} A point feature
         * @private
         */
        'point': function(str) {
            var coords = str.trim().split(this.regExes.spaces);
            return L.latLng(coords[1], coords[0]);
        },

        /**
         NOT IMPLEMENTED

         'multipoint': function(str) {
         var point;
         var points = str.split(',');
         var components = [];
         for(var i=0, len=points.length; i<len; ++i) {
         point = points[i].replace(this.regExes.trimParens, '$1');
         components.push(this.parse.point.apply(this, [point]).geometry);
         }
         return new OpenLayers.Feature.Vector(
         new OpenLayers.Geometry.MultiPoint(components)
         );
         },*/

        /**
         * Return a linestring feature given a linestring WKT fragment.
         * @param {String} str A WKT fragment representing the linestring
         * @returns {L.Polyline} A linestring feature
         * @private
         */
        'linestring': function(str) {
            var points = str.split(',');
            var components = [];
            for(var i=0, len=points.length; i<len; ++i) {
                components.push(this.parse.point.apply(this, [points[i]]));
            }
            return L.polyline(components);
        },

        /**
         NOT IMPLEMENTED

        'multilinestring': function(str) {
            var line;
            var lines = OpenLayers.String.trim(str).split(this.regExes.parenComma);
            var components = [];
            for(var i=0, len=lines.length; i<len; ++i) {
                line = lines[i].replace(this.regExes.trimParens, '$1');
                components.push(this.parse.linestring.apply(this, [line]).geometry);
            }
            return new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.MultiLineString(components)
            );
        },*/

        /**
         * Return a polygon feature given a polygon WKT fragment.
         * @param {String} str A WKT fragment representing the polygon
         * @returns {L.Polygon} A polygon feature
         * @private
         */
        'polygon': function(str) {
            var ring, linestring, linearring;
            var rings = str.split(this.regExes.parenComma);
            var components = [];
            for(var i=0, len=rings.length; i<len; ++i) {
                ring = rings[i].replace(this.regExes.trimParens, '$1');
                linestring = this.parse.linestring.apply(this, [ring])._latlngs;
                components.push(linestring.slice(0, linestring.length - 1));
            }
            return L.polygon(components);
        },

        /**
         * Return a multipolygon feature given a multipolygon WKT fragment.
         * @param {String} str A WKT fragment representing the multipolygon
         * @returns {L.MultiPolygon} A multipolygon feature
         * @private
         */
        'multipolygon': function(str) {
            var polygon,
                leafletPolygon;
            var polygons = str.split(this.regExes.doubleParenComma);
            var components = [];
            for(var i=0, len=polygons.length; i<len; ++i) {
                polygon = polygons[i].replace(this.regExes.trimParens, '$1');
                leafletPolygon = this.parse.polygon.apply(this, [polygon]);
                components.push([leafletPolygon.getLatLngs()].concat(leafletPolygon._holes));
            }
            return L.multiPolygon(components);
        },

        /**
         * Return an array of features given a geometrycollection WKT fragment.
         * @param {String} str A WKT fragment representing the geometrycollection
         * @returns {Array} array of features
         * @private
         */
        'geometrycollection': function(str) {
            // separate components of the collection with |
            str = str.replace(/,\s*([A-Za-z])/g, '|$1');
            var wktArray = str.split('|');
            var components = [];
            for(var i=0, len=wktArray.length; i<len; ++i) {
                components.push(L.WKT.prototype.read.apply(this,[wktArray[i]]));
            }
            return components;
        }

    }
});