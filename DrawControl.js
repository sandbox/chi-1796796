L.Draw = {};
L.DrawHandler = L.Handler.extend({
    statics:{
        convertFeatureToDrawable: function(map, feature){
            if(feature instanceof L.LatLng)
                return L.Draw.Marker.getDrawMarker(map, feature);
            if(feature instanceof L.Marker)
                return L.Draw.Marker.getDrawMarker(map, feature.getLatLng());
            if(feature instanceof L.Polygon)
                return L.Draw.Polygon.getDrawPolygon(map, feature.getLatLngs());
            if(feature instanceof L.Polyline)
                return L.Draw.Polyline.getDrawPolyline(map, feature.getLatLngs());
        }
    },
    addHooks:function () {
        L.DomEvent.addListener(document.body, 'keyup', this._keyUpHandler, this);
    },
    _keyUpHandler:function (event) {
        //if esc pressed
        if (event.keyCode == 27) {
            this.disable();
            this._map.removeLayer(this._feature);
        }

        //if enter pressed - finish shape
        if (event.keyCode == 13 && this._finishShape) {
            this._finishShape();
        }
    },
    removeHooks:function () {
        L.DomEvent.removeListener(document.body, 'keyup', this._keyUpHandler);
    }
});
L.Draw.Marker = L.DrawHandler.extend({
    statics:{
        getDrawMarker: function(map, latlng){
            return new L.Draw.Marker(map).getDrawMarker(latlng);
        }
    },
    addHooks:function () {
        L.DrawHandler.prototype.addHooks.call(this);
        this._map.on('click', this._onClick, this);
    },
    removeHooks:function () {
        L.DrawHandler.prototype.removeHooks.call(this);
        this._map.off('click', this._onClick, this);
    },
    _onClick:function (event) {
        this._feature = this.getDrawMarker(event.latlng).addTo(this._map);
        this._map.fire('feature-added', {type:'point', feature:this._feature});
        this.disable();
    },
    getDrawMarker: function(latlng){
        var marker = L.marker(latlng, {draggable:true});
        marker
            .on('click',this._markerClickHandler, this)
            .on('dragend', this._fireEditEvent, marker);
        return marker;
    },
    _fireEditEvent:function () {
        this.fire('edit');
    },
    _markerClickHandler: function(event) {
        if(event.originalEvent.ctrlKey) {
            this._map.removeLayer(this._feature);
            this._map.fire('feature-removed', {feature:this._feature});
        }
    }
});
L.Draw.Polyline = L.DrawHandler.extend({
    statics:{
        getDrawPolyline: function(map, latlngs){
            var drawPolyline = new L.Draw.Polyline(map);
            drawPolyline._feature.setLatLngs(latlngs);
            drawPolyline._feature.editing.enable();
            drawPolyline._resetEditMarkersClickHandlers();
            return drawPolyline._feature;
        }
    },
    initialize:function (map, options) {
        L.Handler.prototype.initialize.call(this, map, options);
        this._feature = L.polyline([]).addTo(map);
    },
    addHooks:function () {
        L.DrawHandler.prototype.addHooks.call(this);
        this._map.on('click', this._onClick, this);
        this._map.on('dblclick', this._onDblClick, this);
        this._disableDblClickZoom();
    },
    removeHooks:function () {
        L.DrawHandler.prototype.removeHooks.call(this);
        this._map.off('click', this._onClick);
        this._map.off('dblclick', this._onDblClick);
        this._map.off('mousemove', this._mouseMoveHandler);
        this._map.removeLayer(this._guidePolyline);
        this._restoreDblClickZoom();
    },
    _disableDblClickZoom:function () {
        this._doubleClickZoom = this._map.options.doubleClickZoom;
        this._map.doubleClickZoom.disable();
    },
    _restoreDblClickZoom:function () {
        if (this._doubleClickZoom)
            this._map.doubleClickZoom.enable();
    },
    _updateMarkerHandler:function () {
        var editingMarkers = this._feature.editing._markers;
        // The last marker shold have a click handler to close the polyline
        if (editingMarkers.length > 1) {
            editingMarkers[editingMarkers.length - 1]
                .off('click')
                .on('click', this._finishShape, this);
        }

        // Remove the old marker click handler (as only the last point should close the polyline)
        if (editingMarkers.length > 2) {
            editingMarkers[editingMarkers.length - 2].off('click', this._finishShape);
        }
    },
    _resetEditMarkersClickHandlers:function () {
        var that = this;
        this._feature.editing._markers.forEach(function (marker, index) {
            marker
                .off('click')
                .on('click', that._markerClickHandler, that);
        })
    },
    _markerClickHandler: function(event){
        if(event.originalEvent.ctrlKey) {
            this._map.removeLayer(this._feature);
            this._map.fire('feature-removed',{feature:this._feature});
            this.disable();
        } else {
            L.Handler.PolyEdit.prototype._onMarkerClick.call(this._feature.editing,event)
        }
    },
    _finishShape:function () {
        this.disable();
        this._resetEditMarkersClickHandlers();
        this._map.fire('feature-added', {type:'polyline', feature:this._feature});
    },
    _onClick:function (event) {
        L.DomEvent.stopPropagation(event);
        this._feature.addLatLng(event.latlng);
        if (this._feature.getLatLngs().length == 1) {
            this._map.on('mousemove', this._mouseMoveHandler, this);
        } else {
            this._feature.editing.enable();
            this._feature.editing.updateMarkers();
            this._updateMarkerHandler()
        }
    },
    _onDblClick:function (event) {
        this._map.off('dblclick');
        L.DomEvent.disableClickPropagation(event);
        L.DomEvent.stop(event);
        this._finishShape();
    },
    _mouseMoveHandler:function (event) {
        var polylineCoordinates = this._feature.getLatLngs();
        if (!this._guidePolyline) {
            this._guidePolyline = L.polyline([], {clickable:false}).addTo(this._map);
        }
        this._guidePolyline.setLatLngs([polylineCoordinates[polylineCoordinates.length - 1], event.latlng]);


    },
    getDrawPolyline: function(latlngs){
        var polyline = L.Polyline(latlngs);
        polyline.editing.enable();

    }
});
L.Draw.Polygon = L.Draw.Polyline.extend({
    statics:{
        getDrawPolygon: function(map, latlngs){
            var drawPolygon = new L.Draw.Polygon(map);
            drawPolygon._feature.setLatLngs(latlngs);
            drawPolygon._finishShape();
            return drawPolygon._feature;
        }
    },
    initialize:function (map, options) {
        L.Draw.Polyline.prototype.initialize.call(this, map, options);
        //Save link to polyline
        this._polyline = this._feature;
    },
    _updateMarkerHandler:function () {
        if (this._polyline.getLatLngs().length > 2) {
            this._polyline.editing._markers[0]
                .off('click')
                .on('click', this._finishShape, this);
        }
    },
    _finishShape:function () {
        this._feature = L.polygon(this._polyline.getLatLngs()).addTo(this._map);
        this._feature.editing.enable();
        this._resetEditMarkersClickHandlers();
        this._map.removeLayer(this._polyline)
        this.disable();
        this._map.fire('feature-added', {type:'polygon', feature:this._feature});
    }
});
L.Control.Draw = L.Control.extend({
    options:{
        position:'topleft',
        drawButtons:[
            {
                construct:L.Draw.Marker,
                title:'Draw marker',
                className:'point'
            },
            {
                construct:L.Draw.Polyline,
                title:'Draw polyline',
                className:'polyline'
            },
            {
                construct:L.Draw.Polygon,
                title:'Draw polygon',
                className:'polygon'
            }
        ]
    },

    onAdd:function (map) {
        var className = 'leaflet-control-draw',
            container = L.DomUtil.create('div', className);

        this._map = map;

        this._createDrawButtons(container);
        return container;
    },
    _createDrawButtons:function (container) {
        for (var drawButton in this.options.drawButtons) {
            this._createButton(this.options.drawButtons[drawButton], container);
        }
    },
    _createButton:function (button, container) {
        var link = L.DomUtil.create('a', 'leaflet-draw-button ' + button.className, container);
        link.href = '#';
        link.title = button.title;
        L.DomEvent
            .on(link, 'click', L.DomEvent.stopPropagation)
            .on(link, 'mousedown', L.DomEvent.stopPropagation)
            .on(link, 'dblclick', L.DomEvent.stopPropagation)
            .on(link, 'click', L.DomEvent.preventDefault)
            .on(link, 'click', this._drawButtonClickHandler, {drawControl:this, button:button});

        return link;
    },
    _drawButtonClickHandler:function () {
        if (this.drawControl._currentControl) {
            this.drawControl._currentControl.disable();
        }
        this.drawControl._currentControl = new this.button.construct(this.drawControl._map, {});
        this.drawControl._currentControl.enable();
    }
});

L.control.draw = function (options) {
    return new L.Control.Draw(options);
};